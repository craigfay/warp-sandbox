class Model {
  type : string;
  fields : object;
  associations : object[];

  constructor(type : string, fields : object) {
    this.type = type;
    this.fields = fields;
    this.associations = [];
  }
  
  hasMany(type: string) {
    this.associations.push({
      relationship: 'hasMany',
      type,
    })
  }

  belongsTo(type: string) {
    this.associations.push({
      relationship: 'belongsTo',
      type,
    })
  }
}

export const models = {
  create: function(type : string, fields : object) {
    return new Model(type, fields);
  }
}

