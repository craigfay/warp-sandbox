/**
 * Models API specification
 */

`
The sql models interface is a means of interacting with sql databases with a minimum of engine/syntax specific knowledge.
It aims to be loosely coupled enough that nearly any driver can implement it's interface.

## Defining Models
models.create() returns a model that will eventually be used  by the driver to create a table/collection
it has two properties: name, fields

  (user.model.js)
  const { models } = require('../build');

  const user = models.create('user', {
    firstName: {
      type: 'string',
      maxLength: 100,
      required: true,
    },
    age: {
      type: 'integer',
      required: true,
    },
    email: {
      type: 'string',
      unique: true,
    }
  });

user.hasMany('property');

module.exports = { user };
`