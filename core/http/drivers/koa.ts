/**
 * The Koa driver for the http module
 */

/**
 * Dependencies
 */
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');

import { HttpRequest, RouteHandlerPairs, HttpRequestHandler } from '../interface';
import { response, urlMatch } from '../util';

interface CTX {
  url : string;
  method : string;
  query : object;
  headers : object
  request : { body : string }
  status : number;
  set : (key : string, value : any) => any;
  response : { body : string | object },
}

/**
 * Adapters
 */

/**
 * @todo Document this
 * @todo Make CORS a config option
 * @param routeHandlerPairs
 */
function createRoutes (routeHandlerPairs : RouteHandlerPairs) {
  const routes = Object.keys(routeHandlerPairs);
  
  const handlers = routes.map(function(route) {
    return async function(ctx : CTX, next : () => any) {
      // Check that the incoming request url matches the route pattern
      const segments = urlMatch(route, ctx.url);
      
      if (segments) {
        // Build an HttpRequest
        const request = buildRequest({
          method: ctx.method,
          url: ctx.url,
          segments,
          querystring: ctx.query,
          headers: ctx.headers,
          body: ctx.request.body,
        })

        // Get an HttpResponse from the handler
        const response = await routeHandlerPairs[route](request);
        if (response) {
          // Set Status Code
          ctx.status = response.status;
          // Set Headers
          const headerKeys = Object.keys(response.headers);
          headerKeys.forEach(function(key) {
            const value = response.headers[key];
            ctx.set(key, value);
          });
          // Set Body
          ctx.response.body = response.body;
          return; // Send Response
        }
      }
      await next();
    }
  })
  return handlers;
}

/**
 * @todo Document this
 * @param config
 */
function start(config : { port : number, routes : any, callback : () => any }) {
  
  /**
   * Instantiate Koa
   */
  const app = new Koa();

  /**
   * Allow Cross Origin
   */
  app.use(cors());

  /**
   * Body Parser
   */
  app.use(bodyParser())

  /**
   * Apply routes as middleware
   */
  config.routes.forEach(function(route : HttpRequestHandler) {
    app.use(route);
  })
  
  app.listen(config.port, config.callback);
}

/**
 * Enforce the shape of an HttpRequest
 * @todo maybe remove this
 */
function buildRequest (request : HttpRequest): HttpRequest {
  return request;
}

/**
 * Export Structure
 */
export const http = {
  response,
  server: {
    createRoutes,
    start,
  }
}
