/**
 * Interfaces
 */

export interface HttpDriver {
  server : HttpServer;
  response : (response : HttpResponse) => HttpResponse;
}

export interface HttpServer {
  start : () => any;
  handleRoutes : () => any;
}
 
export interface HttpRequest {
  url : string;
  segments : object;
  querystring: object;
  method? : string;
  headers : object;
  body : string;
}

export interface HttpResponse {
  status : number;
  headers : HttpHeaders;
  body : string | object;
}

interface HttpHeaders {
  [key : string] : string;
}

export interface RequestHeaders {
  string: string;
  object: object;
}

export interface RequestBody {
  isJSON : boolean;
  string: string;
  object: object;
}

export interface HttpRequestHandler {
  (request : HttpRequest): HttpResponse | undefined;
}

export interface RouteHandlerPairs {
  [route : string]: HttpRequestHandler;
}

interface handleRoutes {
  (routeHandlers : RouteHandlerPairs ) : void;
}
