/**
 * Dependencies
 */
const Sequelize = require('sequelize');

/**
 * @todo Create Sequelize friendly models from core models
 * @param models
 */
function fromModels(models : object[]) {
  return Object.values(models);
}

/**
 * Convert field's `type` property
 * @param field
 */
function convertTypeRestraint(field : { type : string, maxLength? : number }) {
  switch (field.type) {
    case 'string': {
      const { type, maxLength, ...rest } = field;
      if (maxLength) {
        return { type: Sequelize.STRING(maxLength), ...rest };
      } else {
        return { type: Sequelize.STRING, ...rest };
      }
    }
    case 'integer': {
      const { type, ...rest } = field;
      return { type: Sequelize.INTEGER, ...rest };
    }
    case 'float': {
      const { type, ...rest } = field;
      return { type: Sequelize.FLOAT, ...rest };
    }
    default: {
      throw new Error(`Unrecognized type ${field.type} is not supported.`);
    }
  }
}

/**
 * Convert a field's `required` property
 * @param field
 */
function convertRequiredRestraint(field : { required? : boolean }) {
  const { required, ...rest } = field;
  if (required) {
    return { allowNull: false, ...rest };
  }
  return { ...rest };
}

/**
 * Convert a field's `unique` property
 * @param field
 */
function convertUniqueRestraint(field : { unique? : boolean }) {
  const { unique, ...rest } = field;
  if (unique) {
    return { unique: true, ...rest };
  }
  return { ...rest };
}

function convertReferences(field : { references? : { model : string, field : string }}) {
  const { references, ...rest } = field;
  if (references) {

  }
}

/**
 * A list of conversions that must be applied to each field
 */
const restraintConversions = [
  convertTypeRestraint,
  convertRequiredRestraint,
  convertUniqueRestraint,
];

function convertField(field : object) {
  let convertedField : object = field;
  restraintConversions.forEach((conversion : any) => {
    convertedField = conversion(convertedField);
  })
  return convertedField;
}

interface FindOptions {
  model?: string
  limit?: number;
}

function createFindMethod(models:[]) {
  return function(options:FindOptions) {
    console.log('options:', options)
    console.log('models:', models)
  }
}

/**
 * Run a model through each of it's conversion steps
 * @param migration
 */
function convertModel(migration : { [key : string] : any }): object {
  
  const sequelizeModel : { [key : string] : any } = {};
  
  const { fields } = migration;

  const fieldNames = Object.keys(fields);
  fieldNames.forEach((name : any) => {
    const newField = convertField(fields[name]);
    sequelizeModel[name] = newField;
  });
  
  return sequelizeModel;
}

interface DBInstance {
  models: { [key: string]: Model },
  find: (options: any) => any;
}

interface Model {
  create: (args: any) => any,
}

/**
 * @todo Apply Migrations
 * @param config
 */
async function start(config : { migrations? : { type : string }[] }) {
  const defaults = {
    operatorsAliases: false,
    // logging: false,
  };

  const sequelize = new Sequelize({ ...defaults, ...config });

  const dbInstance: any = { models: {} }
  
  if (config.migrations) {

    const newModels: { [key: string] : any } = {};

    // Convert Models
    config.migrations.forEach((migration : { type : string }) => {
      const fields = convertModel(migration);
      newModels[migration.type] = fields;
    });
    
    Object.keys(newModels).forEach(name => {

      // Set reference models for foreign keys
      Object.keys(newModels[name]).forEach(function(field : string) {
        if (newModels[name].references) {
          const referenceName = newModels[name].references.model;
          newModels[name].references.model = newModels[referenceName];
        }
      })

      // Define Tables
      dbInstance.models[name] = sequelize.define(name, newModels[name], { freezeTableName: true })
    });

    // Defined db operations
    dbInstance.find = createFindMethod(dbInstance.models);

    // Apply changes
    try {
      await sequelize.sync();
      return dbInstance;
    } 
    catch (e) {
      // Known Errors
      if (e.parent.code == 'ECONNREFUSED') {
        throw new Error('Database connection refused.');
      }
      // Unknown Errors
      throw new Error('Unable to connect to database');
    }
  }
}

export const sql = {
  db: {
    start,
    fromModels,
  }
}