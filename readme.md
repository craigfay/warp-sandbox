## Commands

### General Docker

* List all running containers: `docker ps`
* List all containers: `docker ps -a`
* Start a container in the background: `docker start container-name`
* Stop a running container: `docker stop container-name`
* Restart a running container: `docker restart container-name`
* Remove a container: `docker rm container-name`

### Postgres Container

* Create new pg container: `docker run --name postgres-container -p 5432:5432 -e POSTGRES_PASSWORD=password postgres`
* Start existing pg container: `docker start postgres-container`

### Redis Container
docker run -d -p 6379:6379 redis

### Redis/Node Guide
https://hackernoon.com/using-redis-with-node-js-8d87a48c5dd7