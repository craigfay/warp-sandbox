/**
 * Open Application Boundaries 
 */
warpstone = {};
require('./boundaries/database');
require('./boundaries/httpserver');
