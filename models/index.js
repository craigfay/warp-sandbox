const { user } = require('./user.model');
const { property } = require('./property.model');

module.exports = {
  models: {
    user,
    property,
  }
}
