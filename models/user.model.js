const { models } = require('../build');

/**
 * Definition
 */
const user = models.create('user', {
  id: {
    type: 'integer',
    primaryKey: true,
    autoIncrement: true
  },
  firstName: {
    type: 'string',
    maxLength: 100,
    required: true,
  },
  lastName: {
    type: 'string',
    maxLength: 255,
    required: true,
  },
  age: {
    type: 'integer',
    required: true,
  },
  email: {
    type: 'string',
    email: true,
    unique: true,
  }
});

/**
 * Association
 */
user.hasMany('property');

/**
 * Export
 */
module.exports = { user };