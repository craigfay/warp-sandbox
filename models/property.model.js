const { models } = require('../build');

/**
 * Definition
 */
const property = models.create('property', {
  id: {
    type: 'integer',
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: 'string',
    maxLength: 255,
    required: true,
  },
  address: {
    type: 'string',
    maxLength: 255,
    required: true,
  },
  age: {
    type: 'integer',
    required: true,
  },
  user_id: {
    type: 'integer',
    references: {
      model: 'user',
      field: 'id',
    }
  },
});

/**
 * Association
 */
property.belongsTo('user')

/**
 * Export
 */
module.exports = { property };