/**
 * Application Configuration 
 * 
 */

/**
 * httpserver boundary
 */
const httpserverConfig = {
  port: 3000
}

/**
 * database boundary
 */
const databaseConfig = {
  username: "postgres",
  password: "password",
  database: "postgres",
  host: "0.0.0.0",
  dialect: "postgres"
}

/**
 * Exports
 */
module.exports = {
  httpserverConfig,
  databaseConfig,
}