const { sql } = require('../build');
const { models } = require('../models');
const { databaseConfig } = require('../config');

const migrations = sql.db.fromModels(models);

warpstone.db = sql.db.start({
  ...databaseConfig,
  migrations,
})

/**
 * What is the purpose of a migration?
 * * To Create the proper tables on an empty db?
 * * To track historical structural changes?
 */

 /**
  * What is the purpose of reversing a migration?
  * * To clear out unnecessary data?
  */