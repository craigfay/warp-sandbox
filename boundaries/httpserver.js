const { http } = require('../build');
const { httpserverConfig } = require('../config');

function sayHello(request) {
  return http.response({
    headers: {'Content-Type': 'Application/JSON' },
    body: { message: `Hello, ${request.segments.name}!` },
  });
};

const users = [
  { id: 1, name: 'michael', age: 20 },
  { id: 2, name: 'amanda', age: 27 },
  { id: 3, name: 'arnold', age: 35 },
]

function userById(request) {
  const { id } = request.querystring;
  if (!parseInt(id)) {
    return;
  }
  const user = users.find((user) => user.id == id);
  if (user) {
    return http.response({
      headers: {'content-type': 'application/json'},
      body: user,
    });
  }
}

async function testDB(request) {
  const database = await db;
  console.log(await database.models.user.findOne());
  return http.response({
    body: 'testing'
  })
}

const routes = http.server.createRoutes({
  '/sayhello/:name': sayHello,
  '/users': userById,
  '/testdb': testDB,
});

warpstone.server = http.server.start({
  routes,
  ...httpserverConfig,
});
